/**
 * 
 */
package zqq.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import zqq.annotations.EnjoyAuthowired;
import zqq.annotations.EnjoyController;
import zqq.annotations.EnjoyRequestMapping;
import zqq.annotations.EnjoyRequestParam;
import zqq.service.ZqqService;

/**
 * @author qqz
 *
 */
@EnjoyController
@EnjoyRequestMapping("/zqq")
public class ZqqController
{
	@EnjoyAuthowired("ZqqServiceImpl")
	private ZqqService zqqService;

	@EnjoyRequestMapping("/query")
	public void query(HttpServletRequest req, HttpServletResponse resp, @EnjoyRequestParam("name") String name,
			@EnjoyRequestParam("age") String age)
	{
		PrintWriter pw;
		try
		{
			pw = resp.getWriter();
			String result = zqqService.query(name, age);
			pw.write(result);
		} catch (IOException e)
		{
			e.printStackTrace();
		}

	}
}
