/**
 * 
 */
package zqq.service.impl;

import zqq.annotations.EnjoyService;
import zqq.service.ZqqService;

/**
 * @author qqz
 *
 */
@EnjoyService("ZqqServiceImpl")
public class ZqqServiceImpl implements ZqqService
{

	/*
	 * (non-Javadoc)
	 * 
	 * @see zqq.service.ZqqService#query(java.lang.String, java.lang.String)
	 */
	@Override
	public String query(String name, String age)
	{
		return "{name:" + name + ",age:" + age + "}";
	}

}
